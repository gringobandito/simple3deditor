#version 330 core

out vec4 color;

in vec3 FragPos;  
in vec3 Normal;  
in vec2 TexCoords;
  
uniform vec3 viewPos;


void main()
{
    color = vec4(0.5f, 0.5f, 0.5f, 1.0f);
}