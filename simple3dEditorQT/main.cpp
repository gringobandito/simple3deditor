#include "GLEWImporter.h"
#include "simple3deditor.h"
#include <QtWidgets/QApplication>
#include "QPushButton"
#include <qsurfaceformat.h>
#include "EditorGLWidget.h"
#include "Engine.h"
#include "camera/FreeLookCamera.h"
#include "Scene3D.h"
#include "UpdateBroadcaster.h"
#include "input/MouseInput.h"
#include "input/KeyboardInput.h"
#include "render/RenderModeHelper.h"
#include "BoxModel.h"
#include "GLUtils.h"
#include "Device3D.h"
#include "EditorConfig.h"
#include "QTObjectSelector.h"
#include "ExternalModel.h"

#include <QDebug>
#include <iostream>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
//#include <libproc.h>
//#include <unistd.h>

int main(int argc, char *argv[])
{
    QSurfaceFormat format;
    format.setVersion(3, 3);
    format.setProfile(QSurfaceFormat::CoreProfile);
    QSurfaceFormat::setDefaultFormat( format );

    qDebug() << "C++ Style Debug Message";
    std::cout << "Start" << std::endl;
    QApplication a(argc, argv);
    Simple3dEditor w;
    w.show();

    EditorConfig::application = &w;


    glewExperimental = GL_TRUE;
    GLenum res = glewInit();
    glCheckError();



    if (res != GLEW_OK)
    {
        std::cout << "Failed to initialize GLEW" << std::endl;
        return -1;
    }

    glCheckError();

    Engine engine;
    FreeLookCamera camera1;
    float screenWidth = 800.0f;
    float screenHeight = 600.0f;
    camera1.buildProjectionMatrix(screenWidth, screenHeight, 45.0f, 0.1f, 100.0f);
    auto scene = std::make_shared<Scene3D>(Scene3D(&camera1));


    UpdateBroadcaster updateBroadcaster;
    MouseInput mouseInput;
    KeyboardInput keyboardInput;
    RenderModeHelper renderModeHelper;
    QTObjectSelector objectSelector;
    EditorConfig::objectSelector = &objectSelector;
    Device3D::scene3D = scene;
    engine.objectSelector = &objectSelector;

    scene->init();
//    BoxModel box {};
//    std::shared_ptr<BoxModel> box = std::make_shared<BoxModel>(BoxModel());
//    box->setPosition(2, 0, 0);
//    box->init();
//    box->setId(0x00FF00);
//    BoxModel box2;
//    box2.setId(0x0000FF);
//    box2.setPosition(5.0f, 0.0f, 0.0f);
//    BoxModel box3;
//    box3.setId(0xFF0000);
//    box3.setPosition(0.0f, 0.0f, -5.0f);

//    scene->addChild(box);
//    auto bubble = std::make_shared<ExternalModel>(ExternalModel("../assets/models/achievements/bubble.DAE"));
//    bubble->init();
//    scene->addChild(bubble);
    auto shira = std::make_shared<ExternalModel>(ExternalModel("../assets/models/shira/Shira_animation.DAE"));
    shira->init();
    scene->addChild(shira);

//    scene->addChild(&box3);

    w.init();

    return a.exec();
}
