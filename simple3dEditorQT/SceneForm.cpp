#include "SceneForm.h"
#include "ui_scene_form.h"
#include "Device3D.h"
#include "Scene3D.h"

SceneForm::SceneForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SceneForm)
{
    ui->setupUi(this);
    childrenChangedHandlerPtr = WRAP_EVENT_HADNLER(childrenChangedEventHandler);
}

SceneForm::~SceneForm()
{
    delete ui;
}

void SceneForm::init()
{
    updateChildrenList();
    Device3D::scene3D->addEventListener(Pivot3DEvent::CHILDREN_CHANGED, childrenChangedHandlerPtr);
}

void SceneForm::updateChildrenList()
{
    ui->childrenList->clear();
    auto sceneItem = new QTreeWidgetItem((QTreeWidget*)0, QStringList(QString("Scene")));

    ui->childrenList->addTopLevelItem(sceneItem);
    sceneItem->setIcon(0, QIcon("../qt_assets/3d_axes_icon.png"));

    fillChildren(sceneItem, Device3D::scene3D->children());
}

void SceneForm::fillChildren(QTreeWidgetItem* const parentListItem, const pivots_list& children)
{
    for (auto child : children)
    {
        auto childItem = new QTreeWidgetItem((QTreeWidget*)0, QStringList(QString(child->name().c_str())));
        if (dynamic_pointer_cast<Mesh>(child))
        {
            childItem->setIcon(0, QIcon("../qt_assets/mesh_icon.png"));
        }
        else if (dynamic_pointer_cast<ExternalModel>(child))
        {
            childItem->setIcon(0, QIcon("../qt_assets/model_icon.png"));
        }
        else
        {
            childItem->setIcon(0, QIcon("../qt_assets/3d_axes_icon.png"));
        }
        parentListItem->addChild(childItem);
        fillChildren(childItem, child->children());
    }
}

void SceneForm::childrenChangedEventHandler()
{
    updateChildrenList();
}


