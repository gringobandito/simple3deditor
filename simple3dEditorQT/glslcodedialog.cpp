#include "glslcodedialog.h"
#include "ui_glslcodedialog.h"
#include "Shader.h"

GLSLCodeDialog::GLSLCodeDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::GLSLCodeDialog)
{
    ui->setupUi(this);
}

GLSLCodeDialog::~GLSLCodeDialog()
{
    delete ui;
}

void GLSLCodeDialog::init(std::shared_ptr<Shader> shader)
{
    updateShaderCode(shader);
}

void GLSLCodeDialog::updateShaderCode(std::shared_ptr<Shader> shader)
{
    if (shader)
    {
        ui->vertexShaderTF->setText(QString::fromStdString(shader->vertexSource()));
        ui->fragmentShaderTF->setText(QString::fromStdString(shader->fragmentSource()));
    }
}
