#ifndef SCENEFORM_H
#define SCENEFORM_H

#include <QWidget>
#include "Pivot3D.h"
#include "events/EventDispatcher.h"

class QTreeWidgetItem;

namespace Ui {
class SceneForm;
}

class SceneForm : public QWidget
{
    Q_OBJECT
private:
    Ui::SceneForm *ui;
    listenerFuncPtr childrenChangedHandlerPtr;
public:
    explicit SceneForm(QWidget *parent = 0);
    ~SceneForm();
    void init();
    void updateChildrenList();
private:
    void fillChildren(QTreeWidgetItem* const parentListItem, const pivots_list& children);
    void childrenChangedEventHandler();
};

#endif // SCENEFORM_H
