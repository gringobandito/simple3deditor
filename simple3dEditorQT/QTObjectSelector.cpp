#include "QTObjectSelector.h"
#include "input/MouseInput.h"
#include <GL/glew.h>
#include "Device3D.h"
#include "EditorConfig.h"
#include <QtCore/QPoint>
#include <iostream>
#include "QImage"
//#include "QOpenGLContext"
//#include "QOpenGLFunctions"
#include "QOpenGLFramebufferObject"

QTObjectSelector::QTObjectSelector()
	:ObjectSelector()
{
}

QTObjectSelector::~QTObjectSelector()
{

}

void QTObjectSelector::onPreRender()
{
	if (_needRender) {
		ObjectSelector::pickObject();
		_needRender = false;
	}
}

void QTObjectSelector::pickObject()
{
	_needRender = true;
}
