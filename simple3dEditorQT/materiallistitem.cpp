#include "materiallistitem.h"

#include "materials/MaterialBase.h"

MaterialListItem::MaterialListItem(QListWidget *parent) : QListWidgetItem(parent)
{

}

std::shared_ptr<MaterialBase> MaterialListItem::material() const
{
    return _material;
}

void MaterialListItem::setMaterial(std::shared_ptr<MaterialBase> material)
{
    _material = material;
}
