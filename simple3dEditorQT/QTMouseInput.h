#pragma once
#include "input/MouseInput.h"
#ifndef QTMouseInput_h__
#define QTMouseInput_h__

class QMainWindow;

class QTMouseInput: public MouseInput
{
private:
	QMainWindow* _window;

public:
	QTMouseInput(QMainWindow* window);
	~QTMouseInput();
	static void cursorPositionCallback(double xpos, double ypos);
	static void mouseButtonCallback(int button, int action, int mods);
	static void scrollCallback(double xoffset, double yoffset);
protected:
	
private:
};

#endif // QTMouseInput_h__
