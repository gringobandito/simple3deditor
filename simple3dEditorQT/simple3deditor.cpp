#include "simple3deditor.h"
#include "ui_simple3deditor.h"
#include "EditorConfig.h"
#include "QTObjectSelector.h"
#include "Pivot3D.h"
#include <glm/glm.hpp>

#include <QMessageBox>
#include <QDebug>
#include <QFileDialog>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <iostream>
#include "Device3D.h"
#include "Scene3D.h"
#include "camera/Camera.h"
#include "Mesh.h"
#include <string>
#include "Model.h"
#include "materials/ColorMaterial.h"
#include "materials/MaterialBase.h"
#include "Material3D.h"
#include "materials/ShaderFactory.h"
#include "Mesh.h"
#include "BoxModel.h"
#include "utils/Math.h"
#include "materiallistitem.h"
#include "filter3dwidget.h"
#include <iostream>
#include "glslcodedialog.h"
#include "testform.h"
#include "materials/Filter3d.h"
#include "materials/filters/ColorFilter.h"
#include "colorfilter3dwidget.h"
#include "materials_form.h"
#include "events/Event.h"
#include "SceneForm.h"

Simple3dEditor::Simple3dEditor(QWidget *parent)
    : QMainWindow(parent),
      EventDispatcher(),
     ui(new Ui::Simple3dEditor),
     _glslCodeDialog(nullptr),
     _testDialog(nullptr)
{
    ui->setupUi(this);
    _sceneForm = new SceneForm();
    _materialForm = new MaterialsForm();
    ui->tabWidget->addTab(_sceneForm, "Scene");
    ui->tabWidget->addTab(_materialForm, "Materials");
    //TODO create materials tab
    connect(_materialForm, &MaterialsForm::onMaterialUpdated, this, &Simple3dEditor::updateGLSLCodeDialog);
}

Simple3dEditor::~Simple3dEditor()
{
    delete _materialForm;
    delete _sceneForm;
    delete ui;
}

void Simple3dEditor::init()
{
    EditorConfig::objectSelector->addSelectListener(this);
    _materialForm->init();
    _sceneForm->init();


}

void Simple3dEditor::on_actionSave_as_triggered()
{
    QString fileName = QFileDialog::getSaveFileName(this,
        tr("Save scene"), "",
        tr("JSON (*.json);;All Files (*)"));

    if (fileName.isEmpty())
        return;
    else {
        QFile file(fileName);
        if (!file.open(QIODevice::WriteOnly)) {
            QMessageBox::information(this, tr("Unable to open file"),
                file.errorString());
            return;
        }
        else {
            QJsonObject mainObject;
            mainObject["wireframe"] = true;
            mainObject["clearColor"] = "0x00ff00";

            QJsonObject sceneObject;
            QJsonArray sceneChildren;
            for (auto child : Device3D::scene3D->children()) {
                QJsonObject childObject;

                QJsonObject positionObject;
                positionObject["x"] = child->getPosition()->x;
                positionObject["y"] = child->getPosition()->y;
                positionObject["z"] = child->getPosition()->z;
                QJsonObject rotationObject;
                rotationObject["x"] = child->getRotation()->x;
                rotationObject["y"] = child->getRotation()->y;
                rotationObject["z"] = child->getRotation()->z;
                QJsonObject scaleObject;
                scaleObject["x"] = child->getScale()->x;
                scaleObject["y"] = child->getScale()->y;
                scaleObject["z"] = child->getScale()->z;
                childObject["position"] = positionObject;
                childObject["rotation"] = rotationObject;
                childObject["scale"] = scaleObject;

                //TODO optimize it
                //TODO meshes now Pivots
//                if (!child->meshes().empty())
//                {
//                    QJsonArray childMeshes;
//                    for (auto mesh : child->meshes())
//                    {
//                        QJsonObject meshObject;
//                        QJsonArray vertices;
//                        for (auto vertex : mesh->vertices)
//                        {
//                            vertices.push_back(vertex.Position.x);
//                            vertices.push_back(vertex.Position.y);
//                            vertices.push_back(vertex.Position.z);
//                            vertices.push_back(vertex.Normal.x);
//                            vertices.push_back(vertex.Normal.y);
//                            vertices.push_back(vertex.Normal.z);
//                            vertices.push_back(vertex.TexCoords.x);
//                            vertices.push_back(vertex.TexCoords.y);
//                        }
//                        meshObject["vertices"] = vertices;
//                        QJsonArray indices;
//                        for (auto index : mesh->indices)
//                        {
//                            indices.push_back(static_cast<qint64>(index));
//                        }
//                        meshObject["indices"] = indices;
//                        childMeshes.push_back(meshObject);
//                        //TODO Material
//                    }
//                    childObject["meshes"] = childMeshes;
//                }


                sceneChildren.push_back(childObject);



                //if meshes ...
            }

            //sceneChildren.push_back()


            sceneObject["children"] = sceneChildren;

            mainObject["scene"] = sceneObject;


            QJsonDocument jsonDoc(mainObject);
            file.write(jsonDoc.toJson());
            //                QDataStream out(&file);
            //                        out.setVersion(QDataStream::Qt_4_5);
            //                        out << "contacts";
        }
    }
}
void Simple3dEditor::on_actionOpen_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this,
        tr("Open scene"), "",
        tr("JSON (*.json);;All Files (*)"));

    if (fileName.isEmpty())
        return;
    else {
        QFile file(fileName);
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
            QMessageBox::information(this, tr("Unable to open file"),
                file.errorString());
            return;
        }
        else {
            QString val;
            val = file.readAll();
            file.close();

            QJsonDocument d = QJsonDocument::fromJson(val.toUtf8());
            QJsonObject jsonObject = d.object();



            QJsonObject sceneObject = jsonObject["scene"].toObject();
            QJsonArray sceneChildren = sceneObject["children"].toArray();

            ui->openGLWidget->makeCurrent();

            for (auto childVal : sceneChildren) {
                QJsonObject child = childVal.toObject();
                QJsonArray childMeshes = child["meshes"].toArray();

                //TODO necked pointer, need to delete
                //TODO shared_ptr
                auto model = std::make_shared<Model>(Model());

                for (auto meshVal : childMeshes) {
                    QJsonObject meshInfo = meshVal.toObject();
                    //indices
                    QJsonArray indices = meshInfo["indices"].toArray();
                    std::vector<GLuint> meshIndices;
                    meshIndices.reserve(indices.size());

                    for (int i = 0; i < indices.size(); i++) {
                        meshIndices.push_back(indices[i].toInt());
                    }
                    //vertices
                    QJsonArray vertices = meshInfo["vertices"].toArray();
                    std::vector<Vertex> meshVertices;
                    meshVertices.reserve(meshVertices.size() / 8);

                    for (int i = 0; i < vertices.size(); i += 8) {
                        //position 3f
                        glm::vec3 pos{ vertices[i].toDouble(), vertices[i + 1].toDouble(), vertices[i + 2].toDouble() };
                        glm::vec3 nor{ vertices[i + 3].toDouble(), vertices[i + 4].toDouble(), vertices[i + 5].toDouble() };
                        glm::vec2 tex{ vertices[i + 6].toDouble(), vertices[i + 7].toDouble() };
                        meshVertices.push_back({pos, nor, tex});
                    }

                    auto shader = ShaderFactory::getShader("../assets/shaders/shader.vs",
                                                           "../assets/shaders/defaultColorLight.fs");
                    auto mat = std::make_shared<ColorMaterial>(ColorMaterial(shader));
                    auto mesh = std::make_shared<Mesh>(Mesh(meshVertices, meshIndices, mat, std::vector<VertexBoneData>()));
                    model->addChild(mesh);
                }

                QJsonObject posObject = child["position"].toObject();
                QJsonObject rotObject = child["rotation"].toObject();
                QJsonObject scaleObject = child["scale"].toObject();
                model->setPosition(posObject["x"].toDouble(), posObject["y"].toDouble(), posObject["z"].toDouble());
                model->setRotation(rotObject["x"].toDouble(), rotObject["y"].toDouble(), rotObject["z"].toDouble());
                model->setScale(scaleObject["x"].toDouble(), scaleObject["y"].toDouble(), scaleObject["z"].toDouble());
                model->setId(0x00FF00);
                Device3D::scene3D->addChild(model);

            }
        }
    }
}

void Simple3dEditor::on_actionBox_triggered()
{

    ui->openGLWidget->makeCurrent();
    std::cout << "Create::Box" << std::endl;
    auto box = std::make_shared<BoxModel>(BoxModel());
    box->init();
    //box->setId(0x00FF00);
    box->setPosition(Math::randomF() * 5, 0, 0);
    Device3D::scene3D->addChild(box);
}

void Simple3dEditor::on_actionNew_triggered()
{
    std::cout << "NEW" << std::endl;

    Device3D::scene3D.reset();
    Device3D::scene3D = std::make_shared<Scene3D>(Scene3D(Device3D::camera));
//    Device3D::scene3D = new Scene3D(Device3D::camera);
}

// Window actions
void Simple3dEditor::on_actionGLSL_Code_triggered()
{
    if (!_glslCodeDialog)
    {
        _glslCodeDialog = new GLSLCodeDialog(this);
    }

    _glslCodeDialog->show();
    _glslCodeDialog->raise();
    _glslCodeDialog->activateWindow();
    auto material = _materialForm->getCurrentMaterial();
    if (material)
    {
        _glslCodeDialog->updateShaderCode(material->shader());
    }

}

void Simple3dEditor::on_actionTestDialog_triggered()
{
    if (!_testDialog)
    {
        _testDialog = new TestForm(this);
    }

    _testDialog->show();
    _testDialog->raise();
    _testDialog->activateWindow();

}

void Simple3dEditor::handleSelectedObject(std::shared_ptr<Pivot3D> pivot)
{
    if (pivot) {
        const glm::vec3* pos = pivot->getPosition();
        const glm::vec3* rot = pivot->getRotation();
        const glm::vec3* scale = pivot->getScale();

        ui->position_x->setText(QString::number(pos->x));
        ui->position_y->setText(QString::number(pos->y));
        ui->position_z->setText(QString::number(pos->z));

        ui->rotation_x->setText(QString::number(rot->x));
        ui->rotation_y->setText(QString::number(rot->y));
        ui->rotation_z->setText(QString::number(rot->z));

        ui->scale_x->setText(QString::number(scale->x));
        ui->scale_y->setText(QString::number(scale->y));
        ui->scale_z->setText(QString::number(scale->z));

        ui->objectName->setText(QString::fromStdString(pivot->name()));
    }
    else {
        ui->position_x->clear();
        ui->position_y->clear();
        ui->position_z->clear();

        ui->rotation_x->clear();
        ui->rotation_y->clear();
        ui->rotation_z->clear();

        ui->scale_x->clear();
        ui->scale_y->clear();
        ui->scale_z->clear();
    }
}

void Simple3dEditor::updateSelectedObjectPosition()
{
    auto selectedObject = EditorConfig::objectSelector->getSelectedObject();
    if (selectedObject) {
        float posX = std::stof(ui->position_x->text().toStdString());
        float posY = std::stof(ui->position_y->text().toStdString());
        float posZ = std::stof(ui->position_z->text().toStdString());
        selectedObject->setPosition(posX, posY, posZ);
    }
}

void Simple3dEditor::updateSelectedObjectRotation()
{
    auto selectedObject = EditorConfig::objectSelector->getSelectedObject();
    if (selectedObject) {
        float rotX = std::stof(ui->rotation_x->text().toStdString());
        float rotY = std::stof(ui->rotation_y->text().toStdString());
        float rotZ = std::stof(ui->rotation_z->text().toStdString());
        selectedObject->setRotation(rotX, rotY, rotZ);
    }
}

void Simple3dEditor::updateSelectedObjectScale()
{
    auto selectedObject = EditorConfig::objectSelector->getSelectedObject();
    if (selectedObject) {
        float scaleX = std::stof(ui->scale_x->text().toStdString());
        float scaleY = std::stof(ui->scale_y->text().toStdString());
        float scaleZ = std::stof(ui->scale_z->text().toStdString());
        selectedObject->setScale(scaleX, scaleY, scaleZ);
    }
}

void Simple3dEditor::updateGLSLCodeDialog(const std::shared_ptr<MaterialBase> material)
{
    if (_glslCodeDialog)
    {
        _glslCodeDialog->updateShaderCode(material->shader());
    }
}
