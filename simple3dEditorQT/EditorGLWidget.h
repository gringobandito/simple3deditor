#pragma once
#ifndef EditorGLWidget_h__
#define EditorGLWidget_h__
#include "GLEWImporter.h"
#include <qopenglwidget.h>
#include <memory>
//#include <QOpenGLContext.h>
//#include <QOpenGLFunctions.h>


class QMouseEvent;
class QWheelEvent;
class MaterialBase;
class Pivot3D;
class ColorFilter;

class EditorGLWidget: public QOpenGLWidget
{
private:
    std::shared_ptr<ColorFilter> _selectionFilter = nullptr;
    std::shared_ptr<MaterialBase> _selectionShader = nullptr;
    std::shared_ptr<Pivot3D> _selectedObject = nullptr;
public:
	EditorGLWidget(QWidget *parent);
	void mouseMoveEvent(QMouseEvent * event);
	void mousePressEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent * event);
	void wheelEvent(QWheelEvent * event);
	virtual ~EditorGLWidget();
protected:
	void initializeGL();
	void resizeGL(int w, int h);
	void paintGL();
	

};

#endif // EditorGLWidget_h__
