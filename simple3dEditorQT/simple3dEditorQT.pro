#-------------------------------------------------
#
# Project created by QtCreator 2017-10-16T11:24:43
#
#-------------------------------------------------
VPATH += ../../simple3d/src/
VPATH += ../../simple3d/include/
INCLUDEPATH += ../../simple3d/src/
INCLUDEPATH += ../../simple3d/include/

DEPENDPATH +=   "libs/assimp/Debug/"
DEPENDPATH +=   "libs/SOIL/x64/Debug/"


QT       += core gui opengl
CONFIG += console

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = simple3dEditorQT
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS
DEFINES += GLEW_STATIC

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
    GL/glew.c \
    EditorGLWidget.cpp \
    camera/Camera.cpp \
    camera/FreeLookCamera.cpp \
    input/KeyboardInput.cpp \
    input/MouseInput.cpp \
    materials/ColorMaterial.cpp \
    materials/MaterialBase.cpp \
    materials/ObjectIdMaterial.cpp \
    materials/ShaderFactory.cpp \
    materials/materialshelper.cpp \
    object_selector/ObjectSelector.cpp \
    render/RenderModeHelper.cpp \
    resources/Texture2D.cpp \
    resources/TextureManager.cpp \
    utils/FileUtils.cpp \
    BoxModel.cpp \
    Device3D.cpp \
    Engine.cpp \
    ExternalModel.cpp \
    GLUtils.cpp \
    Material3D.cpp \
    Mesh.cpp \
    Model.cpp \
    Pivot3D.cpp \
    Scene3D.cpp \
    Shader.cpp \
    UpdateBroadcaster.cpp \
    EditorConfig.cpp \
    QTMouseInput.cpp \
    QTObjectSelector.cpp \
    simple3deditor.cpp \
    materiallistitem.cpp \
    glslcodedialog.cpp \
    ../../simple3d/src/materials/Filter3d.cpp \
    ../../simple3d/src/materials/filters/ColorFilter.cpp \
    ../../simple3d/src/utils/StringUtils.cpp \
    testform.cpp \
    filter3dwidget.cpp \
    colorfilter3dwidget.cpp \
    filteruifactory.cpp \
    basefilterwidget.cpp \
    materials_form.cpp \
    texture_map_filter_widget.cpp \
    ../../simple3d/src/materials/filters/TextureMapFilter.cpp \
    ../../simple3d/src/materials/TextureFactory.cpp \
    ../../simple3d/src/events/EventDispatcher.cpp \
    ../../simple3d/src/utils/Math3d.cpp \
    ../../simple3d/src/materials/SkinnedMaterial3D.cpp \
    SceneForm.cpp



HEADERS  += \
    EditorGLWidget.h \
    camera/Camera.h \
    camera/FirstPersonCamera.h \
    camera/FreeLookCamera.h \
    events/IKeyboardListener.h \
    events/IMouseListener.h \
    events/IObjectSelectorListener.h \
    events/IUpdateListener.h \
    input/KeyboardInput.h \
    input/MouseInput.h \
    materials/ColorMaterial.h \
    materials/MaterialBase.h \
    materials/ObjectIdMaterial.h \
    materials/ShaderFactory.h \
    materials/materialshelper.h \
    object_selector/ObjectSelector.h \
    render/RenderModeHelper.h \
    resources/Texture2D.h \
    resources/TextureManager.h \
    utils/FileUtils.h \
    utils/Math.h \
    BoxModel.h \
    Device3D.h \
    Engine.h \
    ExternalModel.h \
    GLEWImporter.h \
    GLUtils.h \
    Material3D.h \
    Mesh.h \
    Model.h \
    Pivot3D.h \
    Scene3D.h \
    Shader.h \
    UpdateBroadcaster.h \
    EditorConfig.h \
    QTMouseInput.h \
    QTObjectSelector.h \
    GLFW/glfw3.h \
    GLFW/glfw3native.h \
    ../../simple3d/include/GL/glew.h \
    ../../simple3d/include/GL/eglew.h \
    ../../simple3d/include/GL/glxew.h \
    ../../simple3d/include/GL/wglew.h \
    simple3deditor.h \
    materiallistitem.h \
    glslcodedialog.h \
    ../../simple3d/src/materials/Filter3d.h \
    ../../simple3d/src/materials/filters/ColorFilter.h \
    ../../simple3d/src/utils/StringUtils.h \
    testform.h \
    filter3dwidget.h \
    colorfilter3dwidget.h \
    filteruifactory.h \
    basefilterwidget.h \
    materials_form.h \
    texture_map_filter_widget.h \
    ../../simple3d/src/materials/filters/TextureMapFilter.h \
    ../../simple3d/src/materials/TextureFactory.h \
    ../../simple3d/src/events/EventDispatcher.h \
    ../../simple3d/src/utils/Math3d.h \
    ../../simple3d/src/materials/SkinnedMaterial3D.h \
    SceneForm.h


FORMS    += simple3deditor.ui \
    glslcodedialog.ui \
    testform.ui \
    colorfilter3dwidget.ui \
    materials_form.ui \
    texture_map_filter_widget.ui \
    scene_form.ui

win32 {
    LIBS += -lopengl32
    LIBS += -lglu32
    LIBS += -L"D:/_Projects/simple3dEditorQT/simple3dEditorQT/libs/assimp/Debug/" -lassimp-vc140-mt
    LIBS += -L"D:/_Projects/simple3dEditorQT/simple3dEditorQT/libs/SOIL/x64/Debug/" -lSOIL
} mac {

    CONFIG-=app_bundle
    LIBS += -framework CoreFoundation
    LIBS += -L"/Users/pasha/projects/simple3deditor/libs/assimp/osx/" -lassimp
    LIBS += -L"/Users/pasha/projects/simple3deditor/libs/SOIL/osx/" -lSOIL
}

RESOURCES += \
    tqassets.qrc
