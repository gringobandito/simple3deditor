#ifndef TEXTURE_MAP_FILTER_WIDGET_H
#define TEXTURE_MAP_FILTER_WIDGET_H

#include <QWidget>
#include "filter3dwidget.h"

namespace Ui {
class TextureMapFilterWidget;
}

class TextureMapFilterWidget : public Filter3DWidget
{
    Q_OBJECT

public:
    explicit TextureMapFilterWidget(QWidget *parent = 0);
    ~TextureMapFilterWidget();
    void updateUi() override;
private:
    Ui::TextureMapFilterWidget *ui;
};

#endif // TEXTURE_MAP_FILTER_WIDGET_H
