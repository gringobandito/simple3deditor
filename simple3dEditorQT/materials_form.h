#ifndef MATERIALS_FORM_H
#define MATERIALS_FORM_H

#include <QWidget>
#include "events/EventDispatcher.h"
#include <memory>
class MaterialBase;

namespace Ui {
class MaterialsForm;
}

class MaterialsForm : public QWidget
{
    Q_OBJECT

public:
    explicit MaterialsForm(QWidget *parent = 0);
    ~MaterialsForm();
    void init();
    const std::shared_ptr<MaterialBase> getCurrentMaterial() const;
private slots:
    void showMaterialContextMenu(const QPoint&);
    void on_materialNameTF_editingFinished();
    void on_materialNew_clicked();
    void on_materialsList_clicked(const QModelIndex &index);
    void on_newFiltersButton_triggered(QAction *arg1);
    void applyMaterialToSelectedObject();
    void rebuildCurrentMaterial();
    void on_cullfaceBox_currentTextChanged(const QString &arg1);

signals:
     void onMaterialUpdated(const std::shared_ptr<MaterialBase> material);
private:
    Ui::MaterialsForm *ui;
    void updateMaterialPropertiesPanel();
    void onSceneMaterialsUpdate();
    listenerFuncPtr materialsUpdatePtr;
};

#endif // MATERIALSFORM_H
