#include "filter3dwidget.h"
#include <iostream>
#include "materials/materialshelper.h"

Filter3DWidget::Filter3DWidget(QWidget *parent) :
    QWidget(parent)
//    ui(new Ui::Filter3DWidget)
{
//    ui->setupUi(this);
    _blendMode = "Add";
}

Filter3DWidget::~Filter3DWidget()
{
//    delete ui;
}

void Filter3DWidget::setName(const std::string &name)
{
    _filterName->setText(QString::fromStdString(name));
}

void Filter3DWidget::setName(const QString& name)
{
    _filterName->setText(name);
}

void Filter3DWidget::setBlendMode(const BlendMode& blendMode, const bool updateUI)
{
    if (_filter3d && blendMode != BlendMode::NONE)
    {
        _blendMode = MaterialsHelper::blendModeToStr(blendMode);
        _filter3d->setBlendMode(blendMode);
        if (updateUI)
        {
            updateUi();
        }
        emit onFilterChanged();
    }
}

void Filter3DWidget::updateUi()
{
    if (_filter3d)
    {
        setName(_filter3d->name());
        QString blendMode = QString::fromStdString(MaterialsHelper::blendModeToStr(_filter3d->blendMode()));
        _blendingBox->setCurrentText(blendMode);
    }
}

void Filter3DWidget::setFilter3d(std::shared_ptr<Filter3D> filter3d)
{
    _filter3d = filter3d;
}

void Filter3DWidget::on_blendingBox_currentIndexChanged(const QString &arg1)
{
    std::cout << "Filter blending changed to: " << arg1.toStdString() << std::endl;
    setBlendMode(MaterialsHelper::stringToBlendMode(arg1.toStdString()));
}
