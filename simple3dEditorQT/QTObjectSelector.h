#pragma once
#ifndef QTObjectSelector_h__
#define QTObjectSelector_h__
#include "object_selector/ObjectSelector.h"

class QTObjectSelector: public ObjectSelector
{
private:
	bool _needRender;
public:
	QTObjectSelector();
	~QTObjectSelector();
	void onPreRender();
protected:
	void pickObject() override;
private:
};

#endif // QTObjectSelector_h__
