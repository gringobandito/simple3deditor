#ifndef FILTER3DWIDGET_H
#define FILTER3DWIDGET_H

#include <QWidget>
#include <QString>
#include "materials/Filter3d.h"
#include <memory>
#include <string>
#include <QLabel>
#include <QComboBox>

//namespace Ui {
//class Filter3DWidget;
//}

class Filter3DWidget : public QWidget
{
    Q_OBJECT

public:
    explicit Filter3DWidget(QWidget *parent = 0);
    ~Filter3DWidget();
    void setName(const std::string& name);
    void setName(const QString& name);
    void setBlendMode(const BlendMode& blendMode, const bool updateUI = false);
    virtual void updateUi();
    void setFilter3d(std::shared_ptr<Filter3D> filter3d);
signals:
    void onFilterChanged();
private slots:
    void on_blendingBox_currentIndexChanged(const QString &arg1);

protected:
//    Ui::Filter3DWidget *ui;
    std::string _blendMode;
    std::shared_ptr<Filter3D> _filter3d;
protected:
    QLabel* _filterName;
    QComboBox* _blendingBox;
};

#endif // FILTER3DWIDGET_H
