#include "materials_form.h"
#include "ui_materials_form.h"
#include "materiallistitem.h"
#include "colorfilter3dwidget.h"
#include "texture_map_filter_widget.h"
#include "materials/MaterialBase.h"
#include "materials/materialshelper.h"

#include "Material3D.h"
#include "Shader.h"
#include "Device3D.h"
#include <memory>
#include "Scene3D.h"
#include "materials/filters/ColorFilter.h"
#include "materials/filters/TextureMapFilter.h"
#include "resources/TextureManager.h"
#include "Mesh.h"
#include <QMenu>
#include <string>
#include "EditorConfig.h"
#include "QTObjectSelector.h"
#include "Engine.h"

MaterialsForm::MaterialsForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MaterialsForm)
{
    ui->setupUi(this);
    ui->materialsList->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->materialsList, SIGNAL(customContextMenuRequested(QPoint)),
            this, SLOT(showMaterialContextMenu(QPoint)));

    QMenu* menu = new QMenu();
    //TODO should I delete QActions ptr's?
    QAction* colorFilterAction = new QAction("Color Filter", this);
    QAction* textureMapFilterAction = new QAction("TextureMap Filter", this);
    menu->addAction(colorFilterAction);
    menu->addAction(textureMapFilterAction);
    ui->newFiltersButton->setMenu(menu);
    ui->newFiltersButton->setVisible(false);

    materialsUpdatePtr = WRAP_EVENT_HADNLER(onSceneMaterialsUpdate);
}

MaterialsForm::~MaterialsForm()
{
    delete ui;
    Device3D::scene3D->removeEventListener(MaterialBase::MATERIAL_UPDATE_EVENT, materialsUpdatePtr);
}

void MaterialsForm::init()
{
    Device3D::scene3D->addEventListener(MaterialBase::MATERIAL_UPDATE_EVENT, materialsUpdatePtr);
    onSceneMaterialsUpdate();
    //TODO fill materials list
}

const std::shared_ptr<MaterialBase> MaterialsForm::getCurrentMaterial() const
{
    auto currentListItem = static_cast<MaterialListItem*>(ui->materialsList->currentItem());
    if (currentListItem)
        return currentListItem->material();

    return nullptr;
}

void MaterialsForm::showMaterialContextMenu(const QPoint &pos)
{
    // Handle global position
    QPoint globalPos = ui->materialsList->mapToGlobal(pos);

    // Create menu and insert some actions
    QMenu myMenu;
    myMenu.addAction("Apply to selection", this, SLOT(applyMaterialToSelectedObject()));
//    myMenu.addAction("Erase",  this, SLOT(eraseItem()));

    // Show context menu at handling position
    myMenu.exec(globalPos);
}

void MaterialsForm::updateMaterialPropertiesPanel()
{
    ui->filtersList->clear();
    MaterialListItem* currentListItem = static_cast<MaterialListItem*>(ui->materialsList->currentItem());
    if (!currentListItem)
    {
        ui->materialNameTF->setText("");
        ui->newFiltersButton->setVisible(false);
        ui->materialNameTF->setVisible(false);
        return;
    }

    ui->materialNameTF->setVisible(true);
    ui->materialNameTF->setText(QString::fromStdString(currentListItem->material()->name()));

    auto material3d = std::dynamic_pointer_cast<Material3D>(currentListItem->material());

    std::string cullFace = MaterialsHelper::cullFaceToStr(material3d->cullFace());
    ui->cullfaceBox->setCurrentText(QString::fromStdString(cullFace));

    for (auto filter : material3d->getFilters())
    {
        //TODO
        Filter3DWidget* filterWidget = nullptr;
        if (filter->name() == "colorFilter")
        {
            filterWidget = new ColorFilter3dWidget();
        }
        else if (filter->name() == "textureMapFilter")
        {
            filterWidget = new TextureMapFilterWidget();
        }

        if (!filterWidget)
            continue;

        connect(filterWidget, &Filter3DWidget::onFilterChanged, this, &MaterialsForm::rebuildCurrentMaterial);
        QListWidgetItem* item;
        item = new QListWidgetItem(ui->filtersList);
        ui->filtersList->addItem(item);
        item->setSizeHint(filterWidget->minimumSizeHint());
        filterWidget->setFilter3d(filter);
        ui->filtersList->setItemWidget(item, filterWidget);
        filterWidget->updateUi();
    }

    emit onMaterialUpdated(material3d);
    ui->newFiltersButton->setVisible(true);
}

void MaterialsForm::onSceneMaterialsUpdate()
{
    std::cout << "onMaterialsUpdate" << std::endl;
    ui->materialsList->clear();
    auto sceneMaterials = Device3D::scene3D->getMaterials();
    for (auto material : sceneMaterials)
    {
        MaterialListItem* newItem = new MaterialListItem();
        newItem->setMaterial(material);
        newItem->setText(material->name().c_str());
        ui->materialsList->addItem(newItem);
    }
    updateMaterialPropertiesPanel();
}

void MaterialsForm::applyMaterialToSelectedObject()
{
    std::cout << "Apply to selected object" << std::endl;
    auto selectedMesh = std::dynamic_pointer_cast<Mesh>(EditorConfig::objectSelector->getSelectedObject());

    if (selectedMesh != nullptr)
    {
        MaterialListItem* currentListItem = static_cast<MaterialListItem*>(ui->materialsList->currentItem());
        selectedMesh->_material = currentListItem->material();
    }
}

void MaterialsForm::on_materialNameTF_editingFinished()
{
    MaterialListItem* currentListItem = static_cast<MaterialListItem*>(ui->materialsList->currentItem());
    if (!currentListItem)
        return;
    currentListItem->material()->setName(ui->materialNameTF->text().toStdString());
    currentListItem->setText(ui->materialNameTF->text());
    updateMaterialPropertiesPanel();
}

void MaterialsForm::on_materialNew_clicked()
{
    std::cout << "NEW MATERIAL" << std::endl;
    std::shared_ptr<Shader> shader = std::make_shared<Shader>(Shader("../assets/shaders/shader.vs",
                                                                    "../assets/shaders/default.fs"));
    auto m = std::make_shared<Material3D>(Material3D(shader));

    std::string name = "Material ";
    name.append(std::to_string(m->id()));
    m->setName(name);

    m->build();
    Device3D::scene3D->addMaterial(m);

    MaterialListItem* newItem = new MaterialListItem();
    newItem->setMaterial(m);
    newItem->setText(m->name().c_str());
    ui->materialsList->addItem(newItem);
}

void MaterialsForm::on_materialsList_clicked(const QModelIndex&/*index*/)
{
    updateMaterialPropertiesPanel();
}

void MaterialsForm::on_newFiltersButton_triggered(QAction *arg1)
{
    std::cout << "[New Filter Action] "
              << arg1->text().toStdString()
              << std::endl;
    MaterialListItem* currentMaterialListItem = static_cast<MaterialListItem*>(ui->materialsList->currentItem());
    if (currentMaterialListItem)
    {
        auto material = std::dynamic_pointer_cast<Material3D>(currentMaterialListItem->material());
        std::shared_ptr<Filter3D> filter3d;
        if (arg1->text() == "Color Filter")
        {
            filter3d = std::make_shared<ColorFilter>(ColorFilter());
        }
        else if (arg1->text() == "TextureMap Filter")
        {
            std::string dir = "../assets/textures";
            auto texture = Engine::textureManager->getTexture("checker.png", "texture_diffuse", dir);
            filter3d = std::make_shared<TextureMapFilter>(TextureMapFilter(texture));

        }

        material->addFilter(filter3d);
        material->build();
        updateMaterialPropertiesPanel();
    }
}

void MaterialsForm::rebuildCurrentMaterial()
{
    std::cout << "Simple3dEditor::rebuildCurrentMaterial" << std::endl;
    MaterialListItem* currentListItem = static_cast<MaterialListItem*>(ui->materialsList->currentItem());
    currentListItem->material()->build();

    emit onMaterialUpdated(currentListItem->material());
}

void MaterialsForm::on_cullfaceBox_currentTextChanged(const QString &arg1)
{
    MaterialListItem* currentMaterialListItem = static_cast<MaterialListItem*>(ui->materialsList->currentItem());
    if (currentMaterialListItem)
    {
        const CullFaceMode cullFace = MaterialsHelper::stringToCullFace(arg1.toStdString());
        currentMaterialListItem->material()->setCullFace(cullFace);
    }
}
