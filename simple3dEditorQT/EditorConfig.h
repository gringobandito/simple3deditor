#pragma once

#ifndef EditorConfig_h__
#define EditorConfig_h__

class QTObjectSelector;
class Simple3dEditor;

class EditorConfig
{
public:
	static Simple3dEditor* application;
	static QTObjectSelector* objectSelector;
};


#endif // EditorConfig_h__