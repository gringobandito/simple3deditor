#include "colorfilter3dwidget.h"
#include "ui_colorfilter3dwidget.h"
#include <iostream>
#include "materials/materialshelper.h"
#include "materials/filters/ColorFilter.h"

ColorFilter3dWidget::ColorFilter3dWidget(QWidget *parent) :
    Filter3DWidget(parent),
    ui(new Ui::ColorFilter3dWidget)
{
    ui->setupUi(this);
    _filterName = ui->filterName;
    _blendingBox = ui->blendingBox;
}

ColorFilter3dWidget::~ColorFilter3dWidget()
{
    delete ui;
}

void ColorFilter3dWidget::updateUi()
{
    Filter3DWidget::updateUi();
    std::shared_ptr<ColorFilter> colorFilter = std::dynamic_pointer_cast<ColorFilter>(_filter3d);
    if (colorFilter)
    {
        QString color = QString::number(colorFilter->color(), 16);
        ui->colorTextField->setText(color);
    }

}

void ColorFilter3dWidget::on_colorTextField_editingFinished()
{
    unsigned int color = ui->colorTextField->text().toUInt(nullptr, 16);
    std::cout << "ColorFilter3dWidget::on_colorTextField_editingFinished" << std::endl;
    std::cout << "new color value: " << color << std::endl;
    std::shared_ptr<ColorFilter> colorFilter = std::dynamic_pointer_cast<ColorFilter>(_filter3d);
    if (colorFilter)
    {
        colorFilter->setColor(color);
    }
}
