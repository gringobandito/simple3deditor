#include "testform.h"
#include "ui_testform.h"
#include <QMenu>
#include "Device3D.h"
#include "Scene3D.h"
#include <iostream>

TestForm::TestForm(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TestForm)
{
    ui->setupUi(this);
    QMenu *menu = new QMenu();
    QAction *testAction = new QAction("test menu item", this);
    menu->addAction(testAction);
    ui->toolButton->setMenu(menu);
    testEventHandlerPtr = WRAP_EVENT_HADNLER(testEventHandler);
    testEventHandlerPtr2 = WRAP_EVENT_HADNLER(testEventHandler2);
}

TestForm::~TestForm()
{
    delete ui;
}

void TestForm::on_actionColorFilter_triggered()
{

}

void TestForm::on_toolButton_triggered(QAction* /*arg1*/)
{

}

void TestForm::on_pushButton_clicked()
{
//    auto lambda = [this](){testEventHandler();}
    Device3D::scene3D->addEventListener("update", testEventHandlerPtr);
}

void TestForm::testEventHandler()
{
    std::cout << "LALALLALA" << std::endl;
}

void TestForm::testEventHandler2()
{
    std::cout << "FUFUFUFUF" << std::endl;
}

void TestForm::on_pushButton_2_clicked()
{
    Device3D::scene3D->dispatchEvent("update");
}

void TestForm::on_pushButton_3_clicked()
{
    Device3D::scene3D->removeEventListener("update", testEventHandlerPtr);
}


void TestForm::on_pushButton_4_clicked()
{
    if (Device3D::scene3D->hasEventListener("update"))
    {
        std::cout << "HAS" << std::endl;
    }
}

void TestForm::on_pushButton_5_clicked()
{
    Device3D::scene3D->addEventListener("update", testEventHandlerPtr2);
}
