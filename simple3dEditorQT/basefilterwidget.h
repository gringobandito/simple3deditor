#ifndef BASEFILTERWIDGET_H
#define BASEFILTERWIDGET_H
#include <memory>
#include "materials/Filter3d.h"
#include <QObject>


class BaseFilterWidget
{
//    Q_DECLARE_METATYPE(BaseFilterWidget);
//    Q_OBJECT
public:
    BaseFilterWidget();
    ~BaseFilterWidget();
    void setFilter3d(std::shared_ptr<Filter3D> filter3d);

protected:
    std::shared_ptr<Filter3D> _filter3d;
};

#endif // BASEFILTERWIDGET_H
