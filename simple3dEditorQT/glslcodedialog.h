#ifndef GLSLCODEDIALOG_H
#define GLSLCODEDIALOG_H

#include <QDialog>
#include <memory>

namespace Ui {
class GLSLCodeDialog;
}

class Shader;

class GLSLCodeDialog : public QDialog
{
    Q_OBJECT

public:
    explicit GLSLCodeDialog(QWidget *parent = 0);
    ~GLSLCodeDialog();

    void init(std::shared_ptr<Shader> shader);
    void updateShaderCode(std::shared_ptr<Shader> shader);
private:
    Ui::GLSLCodeDialog *ui;
};

#endif // GLSLCODEDIALOG_H
