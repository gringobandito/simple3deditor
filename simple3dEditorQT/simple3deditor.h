#pragma once

#include <QtWidgets/QMainWindow>
#include "events/IObjectSelectorListener.h"
#include "events/EventDispatcher.h"
#include "materials/MaterialBase.h"
#include <memory>
#include "Pivot3D.h"

namespace Ui {
class Simple3dEditor;
}

class Pivot3D;
class MaterialBase;
class QListWidgetItem;
class GLSLCodeDialog;
class TestForm;
class SceneForm;
class MaterialsForm;
class Material3D;


class Simple3dEditor : public QMainWindow, public IObjectSelectorListener, public EventDispatcher
{


    Q_OBJECT
    private slots:
    //File
    void on_actionSave_as_triggered();
    void on_actionNew_triggered();
    void on_actionOpen_triggered();
    //Edit
    //Create
    void on_actionBox_triggered();
    void on_position_x_editingFinished() { updateSelectedObjectPosition(); }
    void on_position_y_editingFinished() { updateSelectedObjectPosition(); }
    void on_position_z_editingFinished() { updateSelectedObjectPosition(); }
    void on_rotation_x_editingFinished() { updateSelectedObjectRotation(); }
    void on_rotation_y_editingFinished() { updateSelectedObjectRotation(); }
    void on_rotation_z_editingFinished() { updateSelectedObjectRotation(); }
    void on_scale_x_editingFinished() { updateSelectedObjectScale(); }
    void on_scale_y_editingFinished() { updateSelectedObjectScale(); }
    void on_scale_z_editingFinished() { updateSelectedObjectScale(); }

    //Materials

    void updateGLSLCodeDialog(const std::shared_ptr<MaterialBase> material);

    void on_actionGLSL_Code_triggered();
    void on_actionTestDialog_triggered();

public:
    Simple3dEditor(QWidget *parent = Q_NULLPTR);
    ~Simple3dEditor();
    void init();
    virtual void handleSelectedObject(std::shared_ptr<Pivot3D> pivot) override;

private:
    Ui::Simple3dEditor* ui;
    GLSLCodeDialog* _glslCodeDialog;
    TestForm* _testDialog;
    SceneForm* _sceneForm;
    MaterialsForm* _materialForm;
    void updateSelectedObjectPosition();
    void updateSelectedObjectRotation();
    void updateSelectedObjectScale();

};
