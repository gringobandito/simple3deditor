#ifndef TESTFORM_H
#define TESTFORM_H

#include <QWidget>
#include <QDialog>
#include "events/EventDispatcher.h"

namespace Ui {
class TestForm;
}

class TestForm : public QDialog
{
    Q_OBJECT

public:
    explicit TestForm(QWidget *parent = 0);
    ~TestForm();

private slots:
    void on_actionColorFilter_triggered();

    void on_toolButton_triggered(QAction *arg1);

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();

private:
    listenerFuncPtr testEventHandlerPtr;
    listenerFuncPtr testEventHandlerPtr2;
    void testEventHandler();
    void testEventHandler2();
    Ui::TestForm *ui;
};

#endif // TESTFORM_H
