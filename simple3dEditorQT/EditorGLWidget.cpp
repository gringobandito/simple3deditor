#include "EditorGLWidget.h"
#include "simple3deditor.h"
#include "Scene3D.h"
#include "Device3D.h"
#include "camera/Camera.h"
#include "qevent.h"
#include <iostream>
#include "QTMouseInput.h"
#include "GLFW/glfw3.h"
#include <QTimer>
#include "EditorConfig.h"
#include "QTObjectSelector.h"
#include "materials/ShaderFactory.h"
#include "Material3D.h"
#include "Mesh.h"
#include "materials/Filter3d.h"
#include "materials/filters/ColorFilter.h"

EditorGLWidget::EditorGLWidget(QWidget *parent)
    : QOpenGLWidget(parent)
{
	this->setMouseTracking(true);
	QTimer *timer = new QTimer(this);
	connect(timer, SIGNAL(timeout()), this, SLOT(update()));
	timer->start(10);

}

void EditorGLWidget::mouseMoveEvent(QMouseEvent * event)
{
	//Show x and y coordinate values of mouse cursor here
	//std::cout << event->x() << " " << event->y() << std::endl;
	QTMouseInput::cursorPositionCallback(event->x(), event->y());
	
}

void EditorGLWidget::mousePressEvent(QMouseEvent *event)
{

	int button = -1;
	if (event->button() == Qt::LeftButton) {
		button = GLFW_MOUSE_BUTTON_LEFT;
	}
	else if (event->button() == Qt::RightButton) {
		button = GLFW_MOUSE_BUTTON_RIGHT;
	}
	else if (event->button() == Qt::MiddleButton) {
		button = GLFW_MOUSE_BUTTON_MIDDLE;
	}

	if (button != -1) {
		std::cout << "button " << button << std::endl;
		QTMouseInput::mouseButtonCallback(button, GLFW_PRESS, 0);
	}

}

void EditorGLWidget::mouseReleaseEvent(QMouseEvent * event)
{
	int button = -1;
	if (event->button() == Qt::LeftButton) {
		button = GLFW_MOUSE_BUTTON_LEFT;
	}
	else if (event->button() == Qt::RightButton) {
		button = GLFW_MOUSE_BUTTON_RIGHT;
	}
	else if (event->button() == Qt::MiddleButton) {
		button = GLFW_MOUSE_BUTTON_MIDDLE;
	}

	if (button != -1) {
		std::cout << "button " << button << std::endl;
		QTMouseInput::mouseButtonCallback(button, GLFW_RELEASE, 0);
	}
}

void EditorGLWidget::wheelEvent(QWheelEvent * event)
{
	QTMouseInput::scrollCallback(0, event->delta() / 120);
}

EditorGLWidget::~EditorGLWidget()
{

}

void EditorGLWidget::initializeGL()
{
	// Set up the rendering context, load shaders and other resources, etc.:
	//QOpenGLFunctions *f = QOpenGLContext::currentContext()->functions();
	//f->glClearColor(0.5f, 1.0f, 0.2f, 1.0f);

    glEnable(GL_DEPTH_TEST);
}

void EditorGLWidget::resizeGL(int w, int h)
{
	// Update projection matrix and other size related settings:

	Device3D::sceenWidth = w;
	Device3D::sceenHeight = h;
	if (Device3D::camera) {
		Device3D::camera->buildProjectionMatrix(w, h, 45.0f, 0.1f, 100.0f);
	}
}

void EditorGLWidget::paintGL()
{
	// Draw the scene:

	if (Device3D::scene3D != nullptr) {
		EditorConfig::objectSelector->onPreRender();

        Device3D::scene3D->render();

        auto selectedObject = EditorConfig::objectSelector->getSelectedObject();

        if (!_selectionFilter)
        {
            _selectionFilter = std::make_shared<ColorFilter>(ColorFilter());
            _selectionFilter->setColor(0xDDDDDD00);
            _selectionFilter->setBlendMode(BlendMode::NORMAL);
        }


        if (_selectedObject != selectedObject)
        {
            _selectedObject = selectedObject;
            auto selectedMesh = std::dynamic_pointer_cast<Mesh>(selectedObject);
            if (selectedMesh)
            {
                _selectionShader = selectedMesh->material()->clone();
                auto material3d = std::dynamic_pointer_cast<Material3D>(_selectionShader);
                material3d->addFilter(_selectionFilter);
                material3d->setCullFace(CullFaceMode::back);
                _selectionShader->build();
            }
        }

        if (selectedObject)
        {
            Device3D::camera = Device3D::scene3D->getCamera();
            Device3D::view = glm::value_ptr(Device3D::scene3D->getCamera()->GetViewMatrix());
            Device3D::projection = Device3D::scene3D->getCamera()->getProjectionMatrix();

            glDepthFunc(GL_LESS);
            glEnable(GL_POLYGON_OFFSET_LINE);
            glPolygonOffset(-1.0f,0.0f);
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            selectedObject->render(_selectionShader);
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            glPolygonOffset(0.0, 0.0);
            glDisable(GL_POLYGON_OFFSET_LINE);
            glDepthFunc(GL_LESS);
        }
	}
}

