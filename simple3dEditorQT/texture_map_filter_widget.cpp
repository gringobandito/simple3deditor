#include "texture_map_filter_widget.h"
#include "ui_texture_map_filter_widget.h"
#include "materials/filters/TextureMapFilter.h"

TextureMapFilterWidget::TextureMapFilterWidget(QWidget *parent) :
    Filter3DWidget(parent),
    ui(new Ui::TextureMapFilterWidget)
{
    ui->setupUi(this);
    _filterName = ui->filterName;
    _blendingBox = ui->blendingBox;

}

TextureMapFilterWidget::~TextureMapFilterWidget()
{
    delete ui;
}

void TextureMapFilterWidget::updateUi()
{
    Filter3DWidget::updateUi();
    std::shared_ptr<TextureMapFilter> filter = std::dynamic_pointer_cast<TextureMapFilter>(_filter3d);
    if (filter)
    {
        auto texture = filter->texture();
        if (texture)
        {
            QPixmap pic(QString::fromStdString(texture->path));
            ui->textureThumb->setPixmap(pic);
        }
    }
}
