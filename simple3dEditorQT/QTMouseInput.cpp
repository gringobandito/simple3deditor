#include "QTMouseInput.h"
#include <QtWidgets/QMainWindow>

QTMouseInput::QTMouseInput(QMainWindow * window)
	:MouseInput(), _window(window)
{
	
}

QTMouseInput::~QTMouseInput()
{
	_window = nullptr;
}

void QTMouseInput::cursorPositionCallback(double xpos, double ypos)
{
	MouseInput::onMouseMove(xpos, ypos);
}

void QTMouseInput::mouseButtonCallback(int button, int action, int /*mods*/)
{
	MouseInput::onMouseButton(button, action);
}

void QTMouseInput::scrollCallback(double xoffset, double yoffset)
{
	MouseInput::onMouseScroll(xoffset, yoffset);
}
