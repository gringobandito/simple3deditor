#ifndef MATERIALLISTITEM_H
#define MATERIALLISTITEM_H

#include <QListWidgetItem>
#include <memory>
class QListWidget;
class MaterialBase;

class MaterialListItem : public QListWidgetItem
{
public:
    explicit MaterialListItem(QListWidget *parent = 0);

signals:

public slots:

private:
    std::shared_ptr<MaterialBase> _material;
public:
    void setMaterial(std::shared_ptr<MaterialBase> material);
    std::shared_ptr<MaterialBase> material() const;
};

#endif // MATERIALLISTITEM_H
