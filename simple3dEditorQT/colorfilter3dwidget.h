#ifndef COLORFILTER3DWIDGET_H
#define COLORFILTER3DWIDGET_H

#include <QWidget>
#include <QString>
#include "materials/Filter3d.h"
#include <memory>
#include <string>
#include "filter3dwidget.h"
namespace Ui {
class ColorFilter3dWidget;
}

class ColorFilter3dWidget : public Filter3DWidget
{
    Q_OBJECT

public:
    explicit ColorFilter3dWidget(QWidget *parent = 0);
    ~ColorFilter3dWidget();
    void updateUi() override;
private slots:
    void on_colorTextField_editingFinished();

private:
    Ui::ColorFilter3dWidget *ui;

};

#endif // COLORFILTER3DWIDGET_H
